"""Vues HTML pour la recherche."""

from http import HTTPStatus
from typing import TYPE_CHECKING

from django.shortcuts import get_object_or_404, redirect, render
from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.vary import vary_on_headers
from django_htmx.http import HTMX_STOP_POLLING, push_url, retarget

from .forms import SearchFilterForm, SearchForm
from .models import Product, Search

if TYPE_CHECKING:
    from django.http import HttpRequest, HttpResponse


@require_GET
def home(request: "HttpRequest") -> "HttpResponse":
    """Accueil."""
    context = {
        "products": Product.objects.order_by("?").all()[:35],
        "form": SearchForm(),
    }
    return render(request, "home.html", context)


@require_POST
@vary_on_headers("HX-Request")
def search(request: "HttpRequest") -> "HttpResponse":
    """Recherche."""
    form = SearchForm(request.POST)
    if form.is_valid():
        search = form.save()
        if not request.htmx:
            redirect(search)
        context = {
            "search": search,
            "products": search.products.prefetch_related("shop"),
            "filter_form": SearchFilterForm(),
        }
        response = render(request, "_shelf.html", context)
        return push_url(response, search.get_absolute_url())
    else:
        context = {"form": form}
        if not request.htmx:
            render(request, "search.html", context)
        response = render(request, "_search_form.html", context)
        return retarget(response, "#search")


@require_GET
@vary_on_headers("HX-Request")
def shelf(request: "HttpRequest", slug: str) -> "HttpResponse":
    """Bac à disques (résultats d'une recherche)."""
    search = get_object_or_404(Search, slug=slug)
    filter_form = SearchFilterForm(request.GET)

    products = search.products.prefetch_related("shop")

    if filter_form.is_valid():
        products = filter_form.filter_products(products)

    status: HTTPStatus | int = HTTPStatus.OK
    context = {
        "search": search,
        "products": products,
    }
    if request.htmx:
        if not search.digging:
            status = HTMX_STOP_POLLING
        template_name = "_products_table.html"
    else:
        context["form"] = SearchForm()
        context["filter_form"] = filter_form
        template_name = "shelf.html"
    return render(request, template_name, context, status=status)


@require_GET
@vary_on_headers("HX-Request")
def product(request: "HttpRequest", slug: str) -> "HttpResponse":
    """Page produit."""
    product = get_object_or_404(Product.objects.select_related("shop"), slug=slug)
    context = {"product": product}
    if request.htmx:
        template_name = "_product.html"
    else:
        context["form"] = SearchForm()
        template_name = "product.html"
    return render(request, template_name, context)
