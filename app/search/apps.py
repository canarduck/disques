"""Search apps."""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class SearchConfig(AppConfig):
    """Search config."""

    name = "search"
    verbose_name = _("recherche")
