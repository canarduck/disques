"""Domino Records."""

import urllib.parse as urlparse

from core.tools import extract_amount
from django.conf import settings
from djmoney.money import Money
from search.diggers import CrateDigger, DigException, logger
from search.models import Product


def get_description(box) -> str:
    """Description depuis une boite produit."""
    description = ""
    desc = box.find("p", class_="description")
    ref = box.find("div", class_="buy").find("h4")
    if ref:
        description = ref.get_text() + " "
    if desc:
        description += desc.get_text()
    return description


def get_title(soup) -> str:
    """Titre depuis les métadonnées ou un header."""
    title = soup.find("meta", property="og:title").get("content")
    try:
        artist = soup.find("span", class_="upperh4").get_text()
        return f"{artist} - {title}"
    except AttributeError:
        return title


def get_image_url(base_url, soup) -> str | None:
    """Image produit."""
    try:
        src = soup.find("div", class_="greyborder").a.img.get("src")
        return f"{base_url}/{src}"
    except AttributeError:
        return None


def get_url(url, buy_link):
    """Url unique du produit."""
    parsed = urlparse.urlparse(buy_link["href"])
    try:
        uid = urlparse.parse_qs(parsed.query).get("item")[0]
    except TypeError:
        # parfois le shop renvoie vers un sous shop
        # type http://franzferdinand.dominomart.com/#9845
        # on utilise le # comme uid
        uid = parsed.fragment

    return f"{url}#{uid}"


class Digger(CrateDigger):
    """Domino Records."""

    base_url = "http://www.dominorecordco.com"
    search_url = "http://www.dominorecordco.com/search/"
    fuzzy_title_ratio = 70

    def search(self):
        """Recherche sur leur moteur.

        2 params à ajouter pour ne chercher que sur le mart
        """
        query = self.query.replace(" - ", " ")
        payload = {"keywords": query, "searchType": 30, "page": "search"}
        soup = self.fetch(self.search_url, payload, verb="post")
        main = soup.find("div", class_="main")
        for link in main.select('a[href^="/uk/"]'):
            logger.debug("Trouvé le lien %s", link["href"])
            yield self.base_url + link["href"]

    def extract(self, url):
        """Extraction des données.

        les releases sont dans div.allReleaseLayers
        """
        logger.info("Extraction des données depuis %s", url)
        soup = self.fetch(url)
        if not soup:
            return
        product = Product()
        product.shop = self.shop
        product.title = get_title(soup)
        product.image_url = get_image_url(self.base_url, soup)
        product.condition = settings.CONDITION_NEW  # que du neuf
        product.availability = settings.AVAILABILITY_IN  # que du stock

        # plusieurs section dans la page
        for box in soup.find_all("div", class_="releaseLayer"):
            media_title = box.find("div", class_="releaseTop").span.get_text()
            if media_title.lower() == "download":
                logger.debug("format numérique")
                continue

            buy_link = box.find("a", class_="cartIconLink")
            if not buy_link:  # pas de lien pour acheter, soldout/dead
                logger.debug("pas de lien pour acheter")
                continue
            product.url = get_url(url, buy_link)
            product.price = Money(extract_amount(buy_link.get_text()), "GBP")
            product.description = get_description(box)

            # les noms des formats sont un petit peu baroques chez domino
            # on essaye de trouver un match dans les classiques
            found_media = ""
            for string, value in self.media_matchings.items():
                if string in media_title.lower():
                    found_media = value
                    break

            if not self.media_match(found_media):
                continue
            product.media = found_media
            yield product

    def search_canary(self) -> bool:
        """Vérification du moteur de recherche."""
        payload = {"keywords": "john"}
        soup = self.fetch(self.search_url, payload, verb="post")
        if not soup:
            return False
        links = soup.select('a[href^="/uk/"]')
        if not links:
            raise DigException("Résultats de la recherche introuvables")
        return True

    def extract_canary(self) -> bool:
        """On prend le 1er lien vers un disque sur l'accueil on teste l'extraction."""
        soup = self.fetch(self.base_url + "/mart/")
        if not soup:
            return False
        link = soup.find("div", id="martRight").select_one('a[href^="/uk/"]')
        self.query = link.get_text(" ", strip=True)  # simule une bonne query
        if not link:
            raise DigException("Lien vers page produit introuvable")
        url = self.base_url + link["href"]
        for _ in self.extract(url):
            return True  # on a une fiche produit
        raise DigException("Rien à extraire de la fiche produit")
