"""Diggers.

Comme des spiders mais pour les disqu.es

Tous les diggers implémentent `CrateDigger` et doivent surclasser les méthodes indiquées.

Pour utiliser les diggers:

```
digger = NormanrecordsDigger(query='Tahiti 80 puzzle', format='vinyl', shop=shop)
results = digger.search()  # retourne [url, url, ...]
infos = digger.extract(results[0])  # retourne un Result

ou

digger = NormanrecordsDigger(query='Tahiti 80 puzzle', format='vinyl', shop=shop)

for result in digger.dig():
    result.save()
```
"""

import logging
import re
from collections.abc import Iterator
from typing import TYPE_CHECKING, Optional

import requests
from bs4 import BeautifulSoup
from django.conf import settings
from rapidfuzz import fuzz

# Permet d'afficher les journaux dans les workers
logger = logging.getLogger(__name__)

charsetprober_logger = logging.getLogger("chardet.charsetprober")
charsetprober_logger.setLevel(logging.INFO)

if TYPE_CHECKING:
    from search.models import Product, Shop


class DigException(Exception):
    """Exception lors du digging du site."""


class CrateDigger:
    """Classe de base pour les diggers.

    propriétés à surclasser
    """

    base_url = "http://disqu.es"  # à surclasser
    search_url = "http://disqu.es"  # à surclasser
    user_agent = settings.USER_AGENT
    fuzzy_title_ratio = 60  # % de match minimal entre titre & requête
    parser = "lxml"
    url_separator = "#"
    proxied = False  # Besoin de passer par tor ?

    def __init__(
        self, query: str, medium: str | None = None, shop: Optional["Shop"] = None
    ):
        """À super() si besoin d'exécuter des opérations particulières.

        ex. changer base_url en fonction du shop, etc.
        """
        logger.info(self.__doc__)
        self.shop = shop
        self.query = query
        if medium is None:
            medium = settings.MEDIA_UNK
        self.medium = medium

    @property
    def media_matchings(self) -> dict:
        """Correspondance entre les noms de formats utilisés.

        ex.
            def media_matchings(self):
                return {'LP': settings.MEDIA_VINYL}
            ...
            self.media_matchings.get('LP', settings.MEDIA_UNKNOWN)
        """
        base = r"(limited|exclusive|deluxe|quadruple|triple|double|((?:\d)(x|))|)"
        return {
            base + r"(lp|vinyl|((5|7|10|12)\"))": settings.MEDIA_VINYL,
            base + r"cd": settings.MEDIA_CD,
            base + r"(tape|cassette|k7)": settings.MEDIA_K7,
        }

    @property
    def availability_matchings(self) -> dict:
        """Correspondance entre les libellés de stocks.

        ex. return {'In stock': AVAILABILITY_IN, 'Not in stock': AVAILABILITY_SO}
        """
        return {}

    def media_guess(self, medium: str) -> str:
        """Retourne le medium qui correspond à ce qu'on trouve sur la page.

        media_guess('Double LP') -> settings.MEDIA_VINYL
        """
        for pattern, match in self.media_matchings.items():
            if re.match(pattern, medium.replace(" ", ""), re.IGNORECASE | re.VERBOSE):
                return match
        return settings.MEDIA_UNK

    def media_match(self, medium: str) -> bool:
        """Test de correspondance du medium.

        Retourne True si :
        * self.medium est None (pas de recherche particulière de format)
        * si ça match dans les regex de media_matchings qui correspondent à self.medium
        """
        logger.debug("Media trouvé: %s, cible: %s", medium, self.medium)
        if self.medium == settings.MEDIA_UNK:
            return True
        return self.medium == self.media_guess(medium)

    def title_match(self, title: str) -> bool:
        """Comparaison fuzzy (partielle) de deux chaînes.

        Retourne True si le résultat est >= `ratio`
        """
        if (
            fuzz.partial_ratio(self.query.lower(), title.lower())
            >= self.fuzzy_title_ratio
        ):
            logger.debug(
                "Fuzzy OK: %s // %s >= %d", self.query, title, self.fuzzy_title_ratio
            )
            return True
        logger.debug(
            "Fuzzy KO: %s // %s < %d", self.query, title, self.fuzzy_title_ratio
        )
        return False

    def product_match_url(self, pid: str, url: str) -> bool:
        """Génération d'une url unique par produit sur une même page.

        Dans une page multi-produit (plusieurs média sur une même page),
        on utilise # (self.url_separator) pour ajouter l'id produit interne (ou
        équivalent) à l’url et l'isoler du reste.
        ex.
        https://www.normanrecords.com/records/129365-dj-shadow-endtroducing#123 -> LP
        https://www.normanrecords.com/records/129365-dj-shadow-endtroducing#234 -> CD
        https://www.normanrecords.com/records/129365-dj-shadow-endtroducing#345 -> box

        Retourne True si :
        * url ne contient pas de #
        * ce qu'il y a après # dans l'url correspond à pid
        """
        logger.debug("Url utilisée: %s, pid: %s", url, pid)
        if self.url_separator not in url:
            return True
        return url.split(self.url_separator)[-1] == pid

    def product_url(self, url: str, pid: str | None = None) -> str:
        """Retourne l'url unique d'un produit (url + # + id produit)."""
        if not pid:
            return url
        return f"{url.split(self.url_separator)[0]}{self.url_separator}{pid}"

    def fetch(
        self, url: str, payload: dict | None = None, verb: str = "get"
    ) -> BeautifulSoup | None:
        """Récupère la page par `requests` et retourne un objet BeautifulSoup.

        On utilise un user_agent aléatoire si aucun n'est spécifié par le digger
        """
        logger.info("Request %s fetch %s", verb.upper(), url)
        logger.debug("Payload %s", payload)
        headers = {"user-agent": self.user_agent}
        logger.debug("Headers %s", headers)

        proxies = (
            {"http": "socks5://tor:9050", "https": "socks5://tor:9050"}
            if self.proxied
            else None
        )
        logger.debug("Proxies %s", proxies)
        if verb == "get":
            page = requests.get(
                url, params=payload, headers=headers, proxies=proxies, timeout=15
            )
            logger.debug("GET %s", page.url)
        else:
            page = requests.post(
                url, data=payload, headers=headers, proxies=proxies, timeout=15
            )
            logger.debug("POST %s", page.url)
        try:
            page.raise_for_status()
            return BeautifulSoup(page.content, self.parser)
        except requests.HTTPError as e:
            logger.error("Erreur HTTP %s", e)
            return None

    def search(self):
        """Méthode de meta recherche sur le site.

        Yield les url à passer à `extract`
        """
        raise NotImplementedError()

    def extract(self, url: str):
        """Extraction des données depuis la page produit.

        Retourne les données extraites sur la page à intégrer dans `Product`
        """
        raise NotImplementedError()

    def _extract(self, url: str):
        """Englobe l'extraction dans un try/except pour enrichir les exceptions."""
        try:
            return self.extract(url)
        except Exception as original_exception:
            original_exception.args += (url,)
            raise

    def dig(self) -> Iterator["Product"]:
        """Lance la recherche et retourne les résultats.

        En cas de surcharger penser à utiliser `_extract` dans la boucle.
        """
        for url in self.search():
            yield from self._extract(url=url)

    def search_canary(self) -> bool:
        """Vérification que la structure du moteur de recherche n'a pas changé."""
        raise NotImplementedError()

    def extract_canary(self) -> bool:
        """Vérification que la structure des pages produit n'a pas changé."""
        raise NotImplementedError()
