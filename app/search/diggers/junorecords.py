"""Juno.co.uk."""

import urllib.parse

from core.helpers import extract_amount
from django.conf import settings
from djmoney.money import Money
from search.diggers import CrateDigger, DigException, logger
from search.models import Product


class Digger(CrateDigger):
    """Juno Records."""

    base_url = "https://www.juno.co.uk"
    search_url = base_url + "/search/"

    @property
    def availability_matchings(self):
        """Correspondance entre les noms de formats utilisés."""
        return {
            "in stock": settings.AVAILABILITY_IN,
            "out of stock": settings.AVAILABILITY_SO,
            "coming soon": settings.AVAILABILITY_PO,
        }

    @property
    def media_matchings(self):
        """Correspondance entre les libellés de stocks."""
        return {
            "vinyl": settings.MEDIA_VINYL,
            "cassette": settings.MEDIA_K7,
            "cd": settings.MEDIA_CD,
            "lp": settings.MEDIA_VINYL,
        }

    def media_guess(self, medium: str) -> str:
        """On cherche des mots clés dans une ligne "format" sur le descriptif du produit.

        ex.
        * cassette limited to 150 copies
        * 180 gram vinyl LP + MP3 download
        """
        for needle, match in self.media_matchings.items():
            if needle in medium.lower():
                return match
        return settings.MEDIA_UNK

    def search(self):
        """Recherche.

        q[all][]=XXX&hide_forthcoming=0&media_type=YYY
        """
        payload = {}
        payload["q[all][]"] = self.query
        payload["hide_forthcoming"] = 0
        # pas de filtre media type k7, ils sont dans vinyl visiblement (?)
        if self.medium == settings.MEDIA_VINYL:
            payload["media_type"] = "vinyl"
        elif self.medium == settings.MEDIA_CD:
            payload["media_type"] = "cd"
        soup = self.fetch(self.search_url, payload)
        for item in soup.find_all("div", class_="dv-item"):
            # 2 liens dans les résultats, un pour l'artiste, l'autre pour le produit
            # l'url commence par /products/
            for link in item.find_all("a", class_="text-md"):
                if "/products/" in link["href"]:
                    url = urllib.parse.urljoin(self.base_url, link["href"])
                    logger.debug("Trouvé le lien %s", url)
                    yield url

    def extract(self, url: str):
        """Un seul disque par page."""
        logger.info("Extraction des données depuis %s", url)
        soup = self.fetch(url)
        if not soup:
            return
        product = Product()
        product.shop = self.shop
        product.url = self.product_url(url)  # un seul produit par page
        # que du neuf
        product.condition = settings.CONDITION_NEW

        artist = soup.find("div", class_="product-artist").get_text(strip=True)
        title = soup.find("div", class_="product-title").get_text(strip=True)
        fulltitle = f"{artist} - {title}"
        if self.query and not self.title_match(fulltitle):
            logger.debug("'%s' ne correspond pas à %s", fulltitle, url)
            return
        product.title = fulltitle

        product.image_url = soup.find("meta", property="og:image").get("content")

        availability_and_price = soup.find("div", class_="product-pricing")
        availability = settings.AVAILABILITY_UNK
        for needle, value in self.availability_matchings.items():
            if needle in availability_and_price.get_text().lower():
                availability = value
                break
        # if availability == settings.AVAILABILITY_SO:
        #     # pas de backorder chez juno, on ignore
        #     return
        product.availability = availability
        price_and_currency = availability_and_price.find(
            "span", class_="product-price"
        ).get_text()
        price = Money(extract_amount(price_and_currency), "EUR")
        if not price:
            logger.info("Prix introuvable sur %s", url)
            return
        product.price = price

        meta = soup.find("div", class_="product-meta")
        # ex. ['Format:', '180 gram vinyl LP', 'Cat:', 'MRG 488LP', ...]
        details = list(meta.stripped_strings)
        found_medium = self.media_guess(details[1])
        if not self.media_match(found_medium):
            logger.info("Média ne match pas")
            return
        product.medium = found_medium
        product.description = " ".join(details)

        yield product

    def search_canary(self) -> bool:
        """Canary de recherche."""
        payload = {}
        payload["q[all][]"] = "dj shadow endtroducing"
        payload["hide_forthcoming"] = "0"
        soup = self.fetch(self.search_url, payload)
        if not soup:
            return False
        items = soup.find_all("div", class_="dv-item")
        if not items:
            raise DigException("Liste des résultats de la recherche introuvable")
        for item in items:
            if not item.select_one("a[href^=/products/]"):
                raise DigException(
                    "Page produit introuvable dans les résultats de la recherche"
                )
        return True

    def extract_canary(self) -> bool:
        """Canary d'extraction.

        On prend le premier lien vers /products/... sur la page dédiée aux bestsellers
        et on teste l'extraction.
        """
        self.fuzzy_title_ratio = 0
        soup = self.fetch(
            self.base_url + "/all/charts/bestsellers/this-week/?media_type=vinyl"
        )
        if not soup:
            return False
        link = soup.select_one("a[href^=/products/]")
        if not link:
            raise DigException("Lien vers page produit introuvable")
        url = self.base_url + link["href"]
        for _ in self.extract(url):
            return True  # on a une fiche produit
        raise DigException("Rien à extraire de la fiche produit")
