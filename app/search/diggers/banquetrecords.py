"""Banquet records.

ex.
* recherche https://www.banquetrecords.com/search?q=king+gizzard
* produit https://www.banquetrecords.com/the-go%21-team/semicircle/MI0477CD
"""

from urllib.parse import urlparse

from django.conf import settings
from djmoney.money import Money
from search.diggers import CrateDigger, DigException, logger
from search.models import Product


class Digger(CrateDigger):
    """Digger Banquet Records."""

    base_url = "https://www.banquetrecords.com"
    search_url = base_url + "/search"
    currency = "GBP"

    def search(self):
        """Recherche sur leur moteur sans filtrer par format.

        Notes:
        - les liens sont relatifs car ils utilisent <base href="">
        - s'il y a des jsessionid=XXX dans les liens (->404), on les retire avec urlparse
        """
        payload = {"q": self.query}
        soup = self.fetch(self.search_url, payload)
        # les résultats contiennent les liens vers les produits dans
        # a.stock-item-image-more-info
        for link in soup.find_all("a", class_="item"):
            logger.debug("Trouvé le lien %s", link["href"])
            yield self.base_url + "/" + urlparse(link["href"]).path

    def extract(self, url: str):
        """Extraction des données.

        Les métadonnées donnent quelques infos sur le produit
        Ensuite tout se trouve dans div#formats > div.format pour chaque option d'achat

        note: le moteur de banquet retourne plein de résultats "liés", on les supprime
        avec une comparaison query / title grace à `title_match`
        """
        logger.info("Extraction des données depuis %s", url)
        soup = self.fetch(url)
        if not soup:
            return

        formats = soup.find("div", id="formats")
        if not formats:
            logger.debug("Pas de format disponible, on ignore")
            return

        for box in formats.find_all("div", class_="format"):
            product = Product()
            product.shop = self.shop

            title = soup.find("meta", property="og:title").get("content")
            if self.query and not self.title_match(title):
                return
            product.title = title
            product.image_url = soup.find("meta", property="og:image").get("content")

            options = box.find("div", class_="options")
            if "sorry" in options.get_text().lower():
                product.availability = settings.AVAILABILITY_SO
            elif "not available" in options.get_text().lower():
                product.availability = settings.AVAILABILITY_SO
            elif soup.find("div", id="notify"):  # note: soup pas box
                product.availability = settings.AVAILABILITY_PO
            else:
                product.availability = settings.AVAILABILITY_IN
            # pid est dans id de div.options : itemFormat123456options
            pid = "".join(c for c in options["id"] if c.isdigit())
            if not self.product_match_url(pid, url):
                logger.info("URL (pid) ne match pas")
                continue
            product.url = self.product_url(url, pid)

            product.condition = settings.CONDITION_NEW  # que du neuf

            media_and_description = box.find("div", class_="name")
            media_label = media_and_description.next_element.strip()
            logger.debug("* Format local : %s", media_label)
            found_media = self.media_guess(str(media_label))
            if not self.media_match(found_media):
                logger.debug("Mauvais format, on ignore")
                continue
            product.media = found_media
            product.description = media_and_description.get_text().strip()

            gbp_price = box.find("div", class_="price").get_text()
            logger.debug("* devise + prix : %s", gbp_price)
            product.price = Money(float(gbp_price[1:]), self.currency)

            yield product

    def search_canary(self) -> bool:
        """Canary."""
        payload = {"q": "field music"}
        soup = self.fetch(self.search_url, payload)
        if not soup:
            return False
        links = soup.find_all("a", class_="item")
        if not links:
            raise DigException("Liste des résultats de la recherche introuvable")
        return True

    def extract_canary(self) -> bool:
        """Canary.

        On prend le premier lien .item sur la home et on teste l'extraction
        """
        soup = self.fetch(self.base_url)
        if not soup:
            return False
        link = soup.find("a", class_="item")
        self.query = link["title"]
        if not link:
            raise DigException("Lien vers page produit introuvable")
        url = self.base_url + "/" + link["href"]
        for _ in self.extract(url):
            return True  # on a une fiche produit
        raise DigException("Rien à extraire de la fiche produit")
