"""Norman records."""

import re
from collections.abc import Iterator

from django.conf import settings
from djmoney.money import Money
from search.diggers import CrateDigger, DigException, logger
from search.models import Product


class Digger(CrateDigger):
    """Digger Norman Records."""

    base_url = "https://www.normanrecords.com"
    search_url = base_url + "/cloudsearch/index.php"
    currency = "GBP"

    @property
    def availability_matchings(self):
        """Disponibilités."""
        return {
            "In stock": settings.AVAILABILITY_IN,
            "Available via supplier": settings.AVAILABILITY_BO,
            "Preorder": settings.AVAILABILITY_PO,
        }

    def search(self) -> Iterator[str]:
        """Recherche sur leur moteur.

        Pas besoin de filtrer par format
        """
        payload = {"q": self.query}
        soup = self.fetch(self.search_url, payload)
        links_set: set[str] = set()
        for link in soup.select('div.browsing_grid a[href^="/records"]'):
            if link["href"] not in links_set:  # dédoublonnage
                logger.info("Trouvé le lien %s", link["href"])
                yield self.base_url + link["href"]
                links_set.add(link["href"])

    def extract(self, url: str):
        """Extraction des données."""
        logger.info("Extraction des données depuis %s", url)
        soup = self.fetch(url)
        if not soup:
            return
        main = soup.find("main")
        for box in main.find_all("div", id=re.compile("item_row_*")):
            product = Product()
            product.shop = self.shop
            product.title = soup.find("meta", property="og:title").get("content")
            product.image_url = soup.find("meta", property="og:image").get("content")
            product.condition = settings.CONDITION_NEW  # que du neuf chez norman

            # uid du produit est dans id de la box ex. item_row_308688
            # cela nous permet d'obtenir une fausse url unique pour le produit
            pid = box["id"].split("_")[-1]
            if not self.product_match_url(pid, url):
                logger.info("URL (pid) ne match pas")
                continue
            product.url = self.product_url(url, f"item_row_{pid}")
            if not self._medium_and_price(box, product):
                continue

            product.availability = settings.AVAILABILITY_UNK
            if soup.find_all("button.btn-group", string="Sold out"):
                product.availability = settings.AVAILABILITY_SO
            else:
                for label in self.availability_matchings.keys():
                    if soup.find_all("strong", string=label):
                        product.availability = settings.AVAILABILITY_UNK
                        break

            # descriptif dans le premier p de la boîte
            product.description = box.find("p").get_text()

            yield product

    def _medium_and_price(self, box, product) -> bool:
        """Extraction média et prix."""
        # Limited Exclusive Vinyl LP
        # <span class="strikethrough">£20.99</span>
        # £16.99
        # <small>OLE1379</small>
        medium_and_price = box.h2
        try:
            medium_and_price.small.decompose()  # on supprime le small (la ref)
        except AttributeError:
            medium = ""  # juste pour éviter une erreur bandit B110
        try:
            medium_and_price.span.decompose()  # on supprime le span (l'ancien prix)
        except AttributeError:
            medium = ""  # juste pour éviter une erreur bandit B110
        try:
            medium, price = medium_and_price.get_text().split("£")
        except ValueError:
            logger.info("Si pas de prix alors ça doit être soldout")
            return False
        found_medium = self.media_guess(medium.strip())
        if not self.media_match(found_medium):
            logger.info("Médium ne match pas")
            return False
        product.medium = found_medium
        product.price = Money(float(price.strip()), self.currency)
        return True

    def search_canary(self) -> bool:
        """Canary."""
        payload = {"q": "joy division"}
        soup = self.fetch(self.search_url, payload)
        if not soup:
            return False
        items = soup.find_all("div", class_="catalogue_item_browse")
        if not items:
            raise DigException("Liste des résultats de la recherche introuvable")
        for item in items:
            if not item.find("a", class_="item_options_link"):
                raise DigException(
                    "Lien vers page produit dans les résultat de la recherche"
                )
        return True

    def extract_canary(self) -> bool:
        """Canary.

        On prend le premier lien vers /records/... sur la home et on teste l'extraction.
        """
        soup = self.fetch(self.base_url)
        if not soup:
            return False
        carousel = soup.find("div", class_="homepage_carousel_new")
        link = carousel.select_one("a[href*=/records/]")

        if not link:
            raise DigException("Lien vers page produit introuvable")
        url = self.base_url + link["href"]
        for _ in self.extract(url):
            return True  # on a une fiche produit
        raise DigException("Rien à extraire de la fiche produit")
