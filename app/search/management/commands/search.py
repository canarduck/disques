"""Commandes django de recherche."""

from argparse import ArgumentParser

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils.translation import gettext_lazy as _
from search.models import Search, Shop


class Command(BaseCommand):
    """CLI de Recherche."""

    help = str(_("Lance une recherche"))

    def add_arguments(self, parser: ArgumentParser) -> None:
        """Arguments."""
        parser.add_argument("query", help=_("requête"))
        parser.add_argument(
            "--shop",
            help=_("disquaire"),
            choices=list(Shop.objects.values_list("slug", flat=True)),
        )

        parser.add_argument(
            "--media",
            help=_("spécifier média"),
            choices=[media[0] for media in settings.MEDIA_CHOICES[1:]],
            default=settings.MEDIA_UNK,
        )

    def handle(self, *args, **options) -> None:
        """Lance la recherche.

        Si --shop est précisé on limite le dig à ce disquaire, sinon on exécute
        sur l'ensemble des disquaires actifs.
        """
        Search.objects.get_or_create(query=options["query"], medium=options["media"])
