"""Commandes django de dig."""

from argparse import ArgumentParser

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils.module_loading import import_string
from django.utils.translation import gettext_lazy as _
from search.models import Shop, digger_choices


class Command(BaseCommand):
    """CLI pour les diggers."""

    help = str(_("Lance un cratedigger"))

    def add_arguments(self, parser: ArgumentParser):
        """Arguments."""
        parser.add_argument(
            "digger",
            help=_("crate digger"),
            choices=[choice[0] for choice in digger_choices()],
        )
        parser.add_argument("query", help=_("requête"))
        parser.add_argument(
            "--shop",
            help=_("disquaire ("),
            choices=list(Shop.objects.values_list("slug", flat=True)),
        )

        parser.add_argument(
            "--medium",
            help=_("spécifier média"),
            choices=[medium[0] for medium in settings.MEDIA_CHOICES[1:]],
            default=settings.MEDIA_UNK,
        )

    def handle(self, *args, **options):
        """Si --shop est précisé on le passe au digger."""
        shop = None
        if options["shop"]:
            shop = Shop.objects.get(slug=options["shop"])
        digger_class = import_string(
            "search.diggers.{name}.Digger".format(name=options["digger"])
        )
        digger = digger_class(
            query=options["query"], medium=options["medium"], shop=shop
        )
        self.stdout.write(
            "Résultats de la recherche de {} ({}) par {}:".format(
                options["query"], options["medium"], options["digger"]
            )
        )
        urls = []
        for index, url in enumerate(digger.search(), 1):
            urls.append(url)
            self.stdout.write(self.style.SUCCESS(f"{index}. {url}"))
        url_index = None
        while urls:
            url_index = input("Extraire quel n° d’url (vide pour quitter) ? ")
            if not url_index:
                break
            url = urls[int(url_index) - 1]
            self.stdout.write(f"Extraction {url}")
            for info in digger.extract(url):
                for key, value in info.__dict__.items():
                    if value is not None:
                        self.stdout.write(f"* {key} = {value}")
                self.stdout.write("-" * 10)
        if not urls:
            self.stdout.write(self.style.NOTICE("Aucun résultat"))
        else:
            self.stdout.write(self.style.SUCCESS("Terminé"))
