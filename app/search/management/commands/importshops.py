"""Fixture pour les disquaires."""

from csv import DictReader

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils.translation import gettext_lazy as _
from search.models import Shop


class Command(BaseCommand):
    """Import des disquaires."""

    help = str(_("Import des disquaires par défaut"))

    def handle(self, *args, **options):
        """Import des disquaires depuis `shops.csv`.

        On `update_or_create` en se basant sur l'url du disquaire.
        """
        shops = settings.FIXTURES_PATH / "shops.csv"
        with shops.open() as csvfile:
            for row in DictReader(csvfile):
                shop, created = Shop.objects.update_or_create(
                    url=row["url"],
                    defaults=row,
                )
                action = "Ajout" if created else "Mise à jour"
                self.stdout.write(self.style.SUCCESS(f"{action} de {shop.name}"))
