"""Paramétrage de disqu.es.

cf. https://docs.djangoproject.com/en/4.0/topics/settings/
"""

from datetime import datetime, timedelta
from email.utils import getaddresses
from pathlib import Path

from environs import Env


def version(base_path: Path, stamp: datetime | None = None) -> str:
    """Numéro de version depuis la date de dernière modif du dossier git.

    Avec repli sur la date passée en paramètre si le dossier git manque.

    Args:
        base_path: chemin vers la racine du projet
        stamp: tampon horaire de repli

    Returns:
        un numéro de version calendaire au format `CALVER_FORMAT`
    """
    git_path = base_path / ".git"

    if git_path.exists():
        stamp = datetime.fromtimestamp(git_path.stat().st_mtime)
    else:
        stamp = datetime.now() if not stamp else stamp

    return stamp.strftime(CALVER_FORMAT)


# Chemins
APP_DIR = Path(__file__).resolve().parent.parent
BASE_DIR = APP_DIR.parent
DIGGERS_PATH = APP_DIR / "search" / "diggers"
FIXTURES_PATH = APP_DIR / "core" / "fixtures"

# Version & environnement
env = Env()  # cf. https://github.com/sloria/environs
env.read_env()
PROJECT_NAME = "disques"
CALVER_FORMAT = "%y.%-m.%-d.%-H%M"
VERSION = version(BASE_DIR)
ENVIRONMENT = env.str("DJANGO_ENVIRONMENT", default="dev")

# Clés API
EBAY_APP_ID = env.str("EBAY_APP_ID")
FIXER_ACCESS_KEY = env.str("FIXER_ACCESS_KEY", default="")

# Application & routage
INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.admin",
    "core",
    "search",
    "djmoney",
    "djmoney.contrib.exchange",
    "widget_tweaks",
    "django_htmx",
    "django_q",
    "sorl.thumbnail",
    "compressor",
    "debug_toolbar",
]
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    "django_htmx.middleware.HtmxMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
]
ROOT_URLCONF = "disques.urls"
WSGI_APPLICATION = "disques.wsgi.application"
APPEND_SLASH = False

# Sécurité, Authentification et debug
SECRET_KEY = env.str("SECRET_KEY")
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", default=["disques.localhost"])
DEBUG = env.bool("DJANGO_DEBUG", default=False)
INTERNAL_IPS = ["127.0.0.1"]
DEBUG_TOOLBAR_CONFIG = {"RESULTS_CACHE_SIZE": 200}
SERVER_EMAIL = env.str("SERVER_EMAIL", default="support@disqu.es")
ADMINS = getaddresses([env.str("ADMINS", default="Admin <admin@disqu.es")])

# Bases de données, cache & tâches de fond
default_sqlite_options = {
    "init_command": (
        "PRAGMA foreign_keys=ON;"
        "PRAGMA journal_mode = WAL;"
        "PRAGMA synchronous = NORMAL;"
        "PRAGMA busy_timeout = 5000;"
        "PRAGMA temp_store = MEMORY;"
        "PRAGMA mmap_size = 134217728;"
        "PRAGMA journal_size_limit = 67108864;"
        "PRAGMA cache_size = 2000;"
    ),
    "transaction_mode": "IMMEDIATE",
}
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "disques.sqlite3",
        "OPTIONS": default_sqlite_options,
    },
    "queue": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "queue.sqlite3",
        "OPTIONS": default_sqlite_options,
    },
}


DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"
Q_CLUSTER = {
    "name": "default",
    "label": "Tâches de fond",
    "workers": 4,
    "timeout": 90,
    "retry": 120,
    "queue_limit": 50,
    "bulk": 10,
    "orm": "queue",
    "sync": DEBUG,
}
CACHES = {
    "default": {
        "BACKEND": "diskcache.DjangoCache",
        "LOCATION": BASE_DIR / "cache",
    },
}

# Internationalisation & devises
LANGUAGE_CODE = "fr"
TIME_ZONE = "Europe/Paris"
USE_I18N = True
USE_TZ = True
BASE_CURRENCY = "EUR"
EXCHANGE_BACKEND = "djmoney.contrib.exchange.backends.FixerBackend"

# Fichiers statiques, médias & templates
PUBLIC_PATH = BASE_DIR / "public"
STATIC_URL = "/static/"
STATIC_ROOT = PUBLIC_PATH / "static"
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "compressor.finders.CompressorFinder",
]
MEDIA_URL = "/media/"
MEDIA_ROOT = PUBLIC_PATH / "media"
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "core.context_processors.config",
            ],
            "builtins": [
                "django.templatetags.static",
                "widget_tweaks.templatetags.widget_tweaks",
                "sorl.thumbnail.templatetags.thumbnail",
            ],
        },
    }
]
TEMPLATE_DEBUG = DEBUG

# Django Compressor
COMPRESS_PRECOMPILERS = [
    ("text/x-scss", "django_libsass.SassCompiler"),
]
COMPRESS_FILTERS = {
    "css": [
        "core.purgecss_filter.PurgeCSSFilter",
        "compressor.filters.css_default.CssAbsoluteFilter",
        "compressor.filters.cssmin.rCSSMinFilter",
    ],
    "js": ["compressor.filters.jsmin.rJSMinFilter"],
}
COMPRESS_PURGECSS_APPS_INCLUDED = ["core", "search"]
COMPRESS_PURGECSS_BINARY = BASE_DIR / "purgecss-cli-linux"
COMPRESS_ROOT = BASE_DIR / "compress"

# Sorl Thumbnails
THUMBNAIL_BACKEND = "core.thumbnail.ThumbnailBackend"
THUMBNAIL_DUMMY = True
THUMBNAIL_DUMMY_SOURCE = "https://placekitten.com/%(width)s/%(height)s"
THUMBNAIL_FORMAT = "WEBP"

# Paramétrage propre aux diggers et autres.
DIGS_TIMEOUT = env.int("DIGS_TIMEOUT", default=60)
DIGS_MAX_FAILURE_WINDOW = timedelta(hours=24)
DIGS_MAX_FAILURE = 10
USER_AGENT = "Disqu.es/" + VERSION
NTFY_TOPIC = env.str("NTFY_TOPIC", default=None)
MEDIA_VINYL = "vinyl"
MEDIA_CD = "cd"
MEDIA_K7 = "cassette"
MEDIA_UNK = "unknown"
MEDIA_CHOICES = [
    (MEDIA_VINYL, "Vinyl"),
    (MEDIA_CD, "CD"),
    (MEDIA_K7, "K7"),
    (MEDIA_UNK, "?"),
]
AVAILABILITY_IN = "stock"
AVAILABILITY_BO = "backorder"
AVAILABILITY_SO = "soldout"
AVAILABILITY_PO = "preorder"
AVAILABILITY_UNK = "unknown"
AVAILABILITY_CHOICES = [
    (AVAILABILITY_IN, "en stock"),
    (AVAILABILITY_BO, "en réapro"),
    (AVAILABILITY_SO, "épuisé"),
    (AVAILABILITY_PO, "pré-commande"),
    (AVAILABILITY_UNK, "?"),
]
CONDITION_NEW = "new"
CONDITION_USED = "used"
CONDITION_UNK = "unknown"
CONDITION_CHOICES = [
    (CONDITION_NEW, "neuf"),
    (CONDITION_USED, "occasion"),
    (CONDITION_UNK, "?"),
]
STATUS_WAIT = "wait"
STATUS_RUNNING = "running"
STATUS_DONE = "done"
STATUS_CANCELLED = "cancel"
STATUS_CHOICES = [
    (STATUS_WAIT, "en attente"),
    (STATUS_RUNNING, "en cours"),
    (STATUS_DONE, "terminé"),
    (STATUS_CANCELLED, "annulé"),
]
REGION_CHOICES = [
    ("AF", "Afrique"),
    ("AS", "Asie"),
    ("CA", "Amérique Centrale"),
    ("EE", "Europe de l’Est"),
    ("WE", "Europe de l’Ouest"),
    ("ME", "Moyen Orient"),
    ("NA", "Amérique du Nord"),
    ("OC", "Océanie"),
    ("SA", "Amérique du Sud"),
    ("TC", "Caraïbes"),
]
