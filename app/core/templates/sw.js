const offlineUrl = "offline.html";
const appVersion = "{{ VERSION }}";

const appShellFiles = [
  '/static/css/style.css',
  '/static/favicon/favicon.ico',
  '/static/images/logo.svg',
];


self.addEventListener('install', (event) => {
  console.log('[Service Worker] Installation.', event);
  event.waitUntil(
    caches.open("cache").then((cache) => {
      console.log('[Service Worker] Mise en cache app shell et contenu.');
      return cache.addAll(appShellFiles);
    })
  );
  self.skipWaiting();
});

self.addEventListener("activate", (event) => {
  console.log('Activation du service worker.', event);
  event.waitUntil(
    self.registration.navigationPreload.enable()
  );
  self.clients.claim();
});

self.addEventListener('fetch', (event) => {
  event.respondWith(
    caches.match(event.request).then((r) => {
      console.log('[Service Worker] Récupération de la ressource: ', event.request.url);
      return r || fetch(event.request).then((response) => {
        /* eslint-disable-next-line security/detect-non-literal-fs-filename */
        return caches.open("cache").then((cache) => {
          console.log('[Service Worker] Mise en cache de la nouvelle ressource: ', event.request.url);
          cache.put(event.request, response.clone());
          return response;
        });
      });
    })
  );
});