"""Boite à outils."""

import re

import requests
from django.conf import settings

# L'ordre est important car recherche "bête" par "in"
# ex. '50CA$' retourne AUD si A$ avant CA$
SYMBOLS = {
    "CA$": "CAD",
    "A$": "AUD",
    "NZ$": "NZD",
    "R$": "BRL",
    "MX$": "MXN",
    "€": "EUR",
    "¥": "JPY",
    "$": "USD",
    "£": "GBP",
    "CHF": "CHF",
    "SEK": "SEK",
    "ZAR": "ZAR",
}


def extract_currency(string: str, fallback: str = "EUR") -> str | None:
    """Utilise SYMBOLS pour identifier la devise dans `string`.

    ex:
    * 50.00 € -> EUR
    * $60.00 -> USD
    * 5 CHF -> CHF
    """
    for symbol, code in SYMBOLS.items():
        if symbol in string:
            return code
    return fallback


def extract_amount(string: str) -> float | None:
    """Trouve un montant dans `string`.

    ex:
    * 50.00 € -> 50.00
    * $60.00 -> 60.00
    * 5 CHF -> 5.00
    """
    matches = re.findall(r"[\d.,]+", string)
    try:
        price = matches[0].replace(",", ".")
        return float(price)
    except (IndexError, ValueError):
        return None


def notify(message: str) -> None:
    """Envoi d'une notification via ntfy."""
    if settings.NTFY_TOPIC:
        requests.post(
            settings.NTFY_TOPIC, data=message.encode(encoding="utf-8"), timeout=15
        )
