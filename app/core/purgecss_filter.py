"""Filtre purgecss pour django compressor.

source: https://github.com/jdbit/django-compressor-purgecss-filter
"""

from pathlib import Path

from compressor.filters import CompilerFilter
from django.conf import settings
from django.template.loaders.app_directories import get_app_template_dirs

COMPRESS_PURGECSS_BINARY = "purgecss"
COMPRESS_PURGECSS_ARGS = ""
COMPRESS_PURGECSS_APPS_INCLUDED: list[str] = []


class PurgeCSSFilter(CompilerFilter):
    """Filtre purgecss."""

    command = "{binary} --css {infile} -o {outfile} {args}"
    options = (
        (
            "binary",
            getattr(settings, "COMPRESS_PURGECSS_BINARY", COMPRESS_PURGECSS_BINARY),
        ),
        ("args", getattr(settings, "COMPRESS_PURGECSS_ARGS", COMPRESS_PURGECSS_ARGS)),
    )

    def __init__(self, *args, **kwargs):
        """Ajout des chemins des gabarits."""
        super().__init__(*args, **kwargs)
        template_files = " ".join(self.get_all_template_files())
        self.command += f" --content {template_files}"

    def get_all_template_files(self) -> list[str]:
        """Détection des dossiers contenant les gabarits."""
        files = []
        apps = getattr(
            settings, "COMPRESS_PURGECSS_APPS_INCLUDED", COMPRESS_PURGECSS_APPS_INCLUDED
        )
        for directory in get_app_template_dirs("templates"):
            if "site-packages" in str(directory):
                for app in apps:
                    if app in str(directory):
                        for f in Path(directory).glob("**/*.html"):
                            files.append(str(f))
            else:
                for f in Path(directory).glob("**/*.html"):
                    files.append(str(f))
        return files
