"""Processeurs de contexte."""

from django.conf import settings
from django.http import HttpRequest


def config(request: HttpRequest) -> dict[str, str]:
    """Donne accès aux templates à certains paramètres."""
    return {
        "VERSION": settings.VERSION,
        "DEBUG": settings.DEBUG,
    }
