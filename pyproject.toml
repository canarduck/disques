[project]
name = "disques"
description = "Un méta-moteur de recherche pour faciliter l'acquisition régulière de disques et l’optimisation des frais de port."
readme = "README.md"
requires-python = ">=3.11"
license = {text = "MIT"}
classifiers = [
    "Framework :: Django :: 4",
    "Programming Language :: Python :: 3 :: Only",
    "Development Status :: 4 - Beta",
    "Operating System :: OS Independent",
    "License :: OSI Approved :: MIT License",
]
dynamic = ["version"]
dependencies = [
    "Django",
    "django-widget-tweaks",
    "environs",
    "django-debug-toolbar",
    "gunicorn",
    "django-htmx",
    "django-bootstrap5",
    # Scrapping
    "requests",
    "beautifulsoup4",
    "django-money[exchange]",
    "html5lib",
    "lxml",
    "ebaysdk",
	"rapidfuzz",
    # Taches de fond
    "django-q2",
    # Traitement des pochettes
    "sorl-thumbnail",
    "pillow",
    # CSS & JS
    "django-compressor",
    "django-libsass",
    # Cache
    "diskcache"
]

[project.optional-dependencies]
dev = [
    "mypy",
    "types-requests",
    "ruff",
    "pytest",
    "pytest-django",
    "pytest-cov",
    "pytest-factoryboy",
    "django-coverage-plugin",
    "freezegun",
]

[build-system]
requires = ["setuptools", "wheel"]
build-backend = "setuptools.build_meta"

[tool.setuptools]
packages = ["app"]

[tool.isort]
profile = "black"

[tool.mypy]
warn_unused_configs = true
ignore_missing_imports = true
warn_unused_ignores = true

[tool.pytest.ini_options]
DJANGO_SETTINGS_MODULE = "disques.settings"
minversion = "6.0"
testpaths = ["tests"]
addopts = """
    app
    --verbose
    --cov=app
    --cov-report=html
    --cov-report=xml
    --cov-report=term-missing:skip-covered
    --junitxml=junit.xml
"""

[tool.coverage.run]
plugins = [
    "django_coverage_plugin",
]
omit = [
    "*/static/*.html",
]
[tool.coverage.django_coverage_plugin]
template_extensions = 'html'

[tool.ruff]
line-length = 89

[tool.ruff.lint]
select = [
    "E", # pycodestyle
    "F", # pyflakes
    "B", # bugbear
    "C90", # mccabe
    "D", # pydocstyle
    "S", # bandit
    "C4", # flake8-comprehensions
    "UP", # pyupgrade
    "T10", # flake8-debugger
    "I", # isort
]
extend-ignore = [
    "D105",  # Missing docstring in magic method
    "D106",  # Missing docstring in public nested class
    "D203",
    "D204",
    "D213",
    "D215",
    "D400",
    "D401",
    "D404",
    "D406",
    "D407",
    "D408",
    "D409",
    "D413",
]

[tool.ruff.lint.per-file-ignores]
# Pas forcément besoin de docstring sur les tests
# Pas besoin de docstring sur les __init__
# Assert dans les tests
"test_*" = ["D100", "D101", "D102", "D103", "D106", "S101", "S105", "S106"]
"factories.py" = ["D100", "D101", "D102", "D103", "D106"]
"__init__.py" = ["D104"]
"*/migrations/*.py" = ["D100", "D101", "E501"]
